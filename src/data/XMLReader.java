package data;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import db.DBConnection;

public class XMLReader {
	
	String pathToFile;
	DBConnection db;
	File xmlFile;
	Element eElement;
	
	int count;
	
	public XMLReader(String pathToFile, DBConnection db) {
		this.pathToFile = pathToFile;
		this.db = db;
		xmlFile = new File(pathToFile);
		count = 0;
	}
	
	public void readXML() throws ParserConfigurationException, SAXException, IOException, DOMException, SQLException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("incidenciaGeolocalizada");
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				eElement = (Element) nNode;
				if ((returnString("fechahora_ini") == null) || (returnString("latitud") == null) || (returnString("longitud") == null)) {
					System.out.println("   Low quality node skipped");
				} else {
					db.addData(returnString("autonomia"),
							returnString("provincia"),
							returnString("poblacion"),
							returnString("causa"),
							returnString("nivel"),
							returnString("tipo"),
							returnString("carretera"),
							returnFloat("pk_inicial"),
							returnFloat("pk_final"),
							returnString("fechahora_ini"),
							returnFloat("latitud"),
							returnFloat("longitud"));
					System.out.println("   Updated node number "+(++count));
				}
			}
		}
	}
	
	public String returnString(String fieldName) {
		String temp = eElement.getElementsByTagName(fieldName).item(0).getTextContent();
		if (temp == null) return "NULL";
		else return temp; 
	}
	
	public float returnFloat(String fieldName) {
		String temp = eElement.getElementsByTagName(fieldName).item(0).getTextContent();
		if (temp == null) return 0;
		else if (temp.equals("")) return 0;
		else return Float.parseFloat(temp);
	}
	
}
