package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnection {

	private Connection connect = null;
	
	public DBConnection(String url, String port, String dbName, String user, String pass)
			throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		connect = DriverManager.getConnection("jdbc:mysql://"+url+":"+port+"/"+dbName, user, pass);
	}
	
	public void addData(String autonomia,
						String provincia,
						String poblacion,
						String causa,
						String nivel,
						String tipo,
						String carretera,
						float kmIni,
						float kmFin,
						String date,
						float lat,
						float lng) {
		int autonomiaID = addAutonomia(autonomia);
		int provinciaID = addProvincia(provincia, autonomiaID);
		int poblacionID = addPoblacion(poblacion, provinciaID);
		int causaID = addSimpleTable(causa, "Causa", "CAUSA");
		int nivelID = addSimpleTable(nivel, "Nivel", "NIVEL");
		int tipoID = addSimpleTable(tipo, "Tipo", "TIPO");
		int carreteraID = addSimpleTable(carretera, "Carretera", "CARRETERA");
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO "
					+ "INCIDENTE(PoblacionID, TipoID, CausaID, NivelID, CarreteraID, KmIni, KmFin, Fecha, Lat, Lng) "
					+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, poblacionID);
			preparedStatement.setInt(2, tipoID);
			preparedStatement.setInt(3, causaID);
			preparedStatement.setInt(4, nivelID);
			preparedStatement.setInt(5, carreteraID);
			preparedStatement.setFloat(6, kmIni);
			preparedStatement.setFloat(7, kmFin);
			preparedStatement.setString(8, date);
			preparedStatement.setFloat(9, lat);
			preparedStatement.setFloat(10, lng);
			preparedStatement.execute();
		} catch (SQLException e) {System.out.println("      Repeated");}
	}
	
	private int addAutonomia(String auto) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO AUTONOMIA(Nombre) VALUES(?)");
			preparedStatement.setString(1, auto);
			preparedStatement.execute();
		} catch (SQLException e) {} finally {
			ResultSet resultSet = null;
			try {
				resultSet = connect.createStatement().executeQuery("SELECT AutonomiaID FROM AUTONOMIA"
																	+ " WHERE Nombre = '"+auto+"'");
				resultSet.next();
				return resultSet.getInt(1);
			} catch (SQLException e) {}
		} return -1;
	}
	
	private int addProvincia(String prov, int auto) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO PROVINCIA(Nombre, AutonomiaID) VALUES(?, ?)");
			preparedStatement.setString(1, prov);
			preparedStatement.setInt(2, auto);
			preparedStatement.execute();
		} catch (SQLException e) {} finally {
			ResultSet resultSet = null;
			try {
				resultSet = connect.createStatement().executeQuery("SELECT ProvinciaID FROM PROVINCIA"
																	+ " WHERE Nombre = '"+prov+"'");
				resultSet.next();
				return resultSet.getInt(1);
			} catch (SQLException e) {}
		} return -1;
	}
	
	private int addPoblacion(String pobl, int prov) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO POBLACION(Nombre, ProvinciaID) VALUES(?, ?)");
			preparedStatement.setString(1, pobl);
			preparedStatement.setInt(2, prov);
			preparedStatement.execute();
		} catch (SQLException e) {} finally {
			ResultSet resultSet = null;
			try {
				resultSet = connect.createStatement().executeQuery("SELECT PoblacionID FROM POBLACION"
																	+ " WHERE Nombre = '"+pobl+"'");
				resultSet.next();
				return resultSet.getInt(1);
			} catch (SQLException e) {}
		} return -1;
	}
	
	public int addSimpleTable(String data, String name, String NAME) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connect.prepareStatement("INSERT INTO "+NAME+"(Nombre) VALUES(?)");
			preparedStatement.setString(1, data);
			preparedStatement.execute();
		} catch (SQLException e) {} finally {
			ResultSet resultSet = null;
			try {
				resultSet = connect.createStatement().executeQuery("SELECT "+name+"ID FROM "+ NAME
																	+ " WHERE Nombre = '"+data+"'");
				resultSet.next();
				return resultSet.getInt(1);
			} catch (SQLException e) {}
		} return -1;
	}
	
	public void closeConnection() {
		try {
			connect.close();
		} catch (SQLException e) {}
	}
	
}