package main;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import data.XMLReader;
import db.ConnData;
import db.DBConnection;

public class Principal {
		
	public Principal() {
		
	}
	
	private void start() {
		DBConnection db = null;
		long mili = System.currentTimeMillis();
		try {
			System.out.println("TRAFFIC DATA UPDATER");
			System.out.print("Connecting to the database... ");
			db = new DBConnection(ConnData.URL, ConnData.PORT, ConnData.DBNAME, ConnData.USERNAME, ConnData.PASSWORD);
			System.out.println(" Connected!");
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println(" ERROR!");
		}
		XMLReader rd = new XMLReader(ConnData.XML, db);
		try {
			System.out.println("Updating data... ");
			rd.readXML();
			System.out.println("Done!");
		} catch (DOMException | ParserConfigurationException | SAXException | IOException | SQLException e) {
			System.out.println("ERROR!");
		}
		db.closeConnection();
		System.out.println("Updater has finished, time: "+(System.currentTimeMillis() - mili)+" miliseconds.");
	}
	
	public static void main(String [] args) {
		Principal prin = new Principal();
		prin.start();
	}

}
